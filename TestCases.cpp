#define CATCH_CONFIG_MAIN

#include "classes.cpp"
#include "catch.hpp"

//testing Film methods
TEST_CASE("Testing set method for Film"){
  Film tst;
  REQUIRE(tst.set("F01", "racism", "4/20/69", "2", "3", "200")==0);
}
TEST_CASE("Testing get method for Film"){
  Film tst;
  REQUIRE(tst.get()==0);
}
TEST_CASE("Testing list method for Film"){
  Film tst;
  REQUIRE(tst.lst()==0);
}


//testing Stand Up methods
TEST_CASE("Testing set method for StandUp"){
  StandUp tst;
  REQUIRE(tst.set("F01", "racism", "4/20/69", "2", "3", "200")==0);
}
TEST_CASE("Testing get method for StandUp"){
  StandUp tst;
  REQUIRE(tst.get()==0);
}
TEST_CASE("Testing list method for StandUp"){
  StandUp tst;
  REQUIRE(tst.lst()==0);
}


//testing Live Music methods
TEST_CASE("Testing set method for LiveMusic"){
  LiveMusic tst;
  REQUIRE(tst.set("F01", "racism", "4/20/69", "2", "3", "200")==0);
}
TEST_CASE("Testing get method for LiveMusic"){
  LiveMusic tst;
  REQUIRE(tst.get()==0);
}
TEST_CASE("Testing list method for LiveMusic"){
  LiveMusic tst;
  REQUIRE(tst.lst()==0);
}
