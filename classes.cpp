#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "classes.h"


// classes.cpp
// Author: M00796216
// Created: 11/1/2022
// Updated: 14/1/2022


int Film::set(std::string a, std::string b, std::string c, std::string d, std::string e, std::string f){
  this->id = a;
  this->name = b;
  this->date = c;
  this->sessions = d;
  this->dimensions = e;
  this->count = f;

  return 0;
}
int Film::get(){
  std::cout<< "ID:" << this->id << std::endl;
  std::cout<< "Name: " << this->name << std::endl;
  std::cout<< "Date of first screening: " << this->date << std::endl;
  std::cout<< "Number of sessions: " << this->sessions << std::endl;
  std::cout<< "Available in: " <<this->dimensions << "D" << std::endl;
  std::cout<< "Seats left: " << this->count << std::endl;

  return 0;
}

int Film::lst(){
  std::cout << "Name: " << this->name << std::endl;
  std::cout << "Date of first screening: " << this->date << std::endl;
  std::cout << "Available in: " << this->dimensions << "D" << std::endl;

  return 0;
}

int StandUp::set(std::string a, std::string b, std::string c, std::string d, std::string e, std::string f){
  this->id = a;
  this->name = b;
  this->date = c;
  this->sessions = d;
  this->comedian = e;
  this->count = f;

  return 0;
}
int StandUp::get(){
  std::cout << "ID: " <<this->id << std::endl;
  std::cout << "Name: " <<this->name << std::endl;
  std::cout << "Date: " <<this->date << std::endl;
  std::cout << "Number of sessions: " << this->sessions << std::endl;
  std::cout << "Comedian: " <<this->comedian << std::endl;
  std::cout << "Seats remaining: " <<this->count << std::endl;

  return 0;
}

int StandUp::lst(){
  std::cout << "Name: " << this->name << std::endl;
  std::cout << "Date: " << this->date << std::endl;
  std::cout << "Comedian: " << this->comedian << std::endl;

  return 0;
}


int LiveMusic::set(std::string a, std::string b, std::string c, std::string d, std::string e, std::string f){
  this->id = a;
  this->name = b;
  this->date = c;
  this->sessions = d;
  this->artist = e;
  this->count = f;

  return 0;
}
int LiveMusic::get(){
  std::cout << "ID: " << this->id << std::endl;
  std::cout << "Name: " << this->name << std::endl;
  std::cout << "Date: " << this->date << std::endl;
  std::cout << "Number of sessions: " << this->sessions << std::endl;
  std::cout << "Artist: " <<this->artist << std::endl;
  std::cout << "Number of seats remaining: " << this->count << std::endl;

  return 0;
}

int LiveMusic::lst(){
  std::cout <<" Name: " << this->name << std::endl;
  std::cout << "Date: " << this->date << std::endl;
  std::cout << "Artist: " << this->artist << std::endl;

  return 0;
}
