#include <iostream>
#include <string>
#include <fstream>

#pragma once

// classes.h
// Author: M00796216
// Created: 13/1/2022
// Updated: 14/1/2022

class Event {
public:
  std::string id=" ";
  std::string name="";
  std::string date="";
  std::string sessions;
  std::string count;
};

class Film: public Event {
  std::string dimensions;
public:
  int set(std::string a, std::string b, std::string c, std::string d, std::string e, std::string f);

  int get();

  int lst();

};


class StandUp: public Event {
  std::string comedian;
public:
  int set(std::string a, std::string b, std::string c, std::string d, std::string e, std::string f);

  int get();

  int lst();

};


class LiveMusic: public Event {
  std::string artist;
public:
  int set(std::string a, std::string b, std::string c, std::string d, std::string e, std::string f);

  int get();

  int lst();
};
