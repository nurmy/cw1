#include <iostream>
#include <string>
#include <vector>
#include "classes.h"


// main.cpp
// Author: M00796216
// Created: 13/1/2022
// Updated: 14/1/2022



// vectors to store objects
std::vector<Film> filmStorage;
std::vector<StandUp> suStorage;
std::vector<LiveMusic> lmStorage;


// functions to create objects
void film(std::vector <std::string> vct){
  // create object
  Film flm;
  // set object's values
  flm.set(vct[0], vct[1], vct[2], vct[3], vct[4], vct[5]);
  // store created object in the global vector
  filmStorage.push_back(flm);
}
void stdUp(std::vector <std::string> vct){
  StandUp stdup;
  stdup.set(vct[0], vct[1], vct[2], vct[3], vct[4], vct[5]);
  suStorage.push_back(stdup);
}
void lm(std::vector <std::string> vct){
  LiveMusic livem;
  livem.set(vct[0], vct[1], vct[2], vct[3], vct[4], vct[5]);
  lmStorage.push_back(livem);
}


// function to update objects
void updt(){
  // clear storages
  filmStorage.clear();
  suStorage.clear();
  lmStorage.clear();

  // access list of eventss from events.txt
  std::ifstream file ("events.txt");
  std::vector <std::string> events;
  std::vector <std::string> files;

  std::string event;
  std::string val;
  while (file >> event){
    events.push_back(event);
  }


  std::string x;
  std::string y;
  std::vector <std::string> temp;
  std::vector <std::string> all;
  int i = 0;
  int j = 6;
  std::string check;

  // construct file to access from contents of events.txt
  for (std::string z : events){
    x = z +".txt";
    // fetch contents of file currently being accessed
    std::ifstream current (x);
    while (current >> val){
      temp.push_back(val);
    }

    std::vector <std::string> toSend;
    check=temp[i];
    for (i; i < j; i++){
      toSend.push_back(temp[i]);
    }

    // check which type of event is being accessed
    char flag = check[0];
    // create appropriate object
    if (flag == 'F'){
      film(toSend);
    }else if (flag == 'S'){
      stdUp(toSend);
    }
    else if (flag == 'L'){
      lm(toSend);
    }

    i = j;
    j = j + 6;
  }
}

// menu option 1
// list all events with basic information
void list(){
  std::cout << "---- Films ----" << std::endl;
  for (Film flm : filmStorage){
    flm.lst();
    std::cout << std::endl;
  }
  std::cout << std::endl;
  std::cout << "---- Stand Ups ----: " << std::endl;
  for (StandUp su : suStorage){
    su.lst();
    std::cout << std::endl;
  }
  std::cout << std::endl;
  std::cout << "---- Live Music shows ----" << std::endl;
  for (LiveMusic lm : lmStorage){
    lm.lst();
    std::cout << std::endl;
  }
}

// menu option 2
int addBooking(){
  std::string id;
  int spots;

  std::cout << "-----------------------------------------" << std::endl;
  std::cout << "ID to book :";
  std::cin >> id;
  std::cout << "-----------------------------------------" << std::endl << std::endl;

  // fetch and open file of event input
  std::string filename = id + ".txt";
  std::ifstream eventfile (filename);
  std::vector <std::string> data;
  std::string info;
  while (eventfile >> info){
    data.push_back(info);
  }

  // display how many spots are available for this event
  std::cout << "-----------------------------------------" << std::endl;
  std::cout << data[data.size()-1]<<" spots available" << std::endl;
  std::cout << "-----------------------------------------" << std::endl;

  // loop until specific answer is given
  std::string response;
  while (1){
    std::cout << ">>> Continue?[Y/N] : ";
    std::cin >> response;
    if ((response == "Y") || (response == "N")){
      break;
    }
}

  // go back if answer to continue is no
  if (response == "N"){
    return 0;
  }

  // check if event type is stand up
  if (id[0] == 'S'){
    // load arrangement file
    std::string code = id +".txt";
    std::ifstream file ("arrangement" + code);
    // array to store all 200 seats
    int seats[200];

    // populate array
    std::string spot;
    int i = 0;
    while (file >> spot){
      // std::cout << spot<< std::endl;
      seats[i] = stoi(spot);
      i++;
    }
    // ask how many seats to book
    std::cout << "How many spots : ";
    std::cin >> spots;

    std::vector <std::string> choices;
    int choice;
    int j = 0;
    // iterate and check if a spot if already booked or not
    while (j < spots){
      std::cout << "Seat number : ";
      std::cin >> choice;
      if (seats[choice-1] == choice){
        seats[choice-1] = -1;
        j++;
      }
      else{
        std::cout << "Seat is already occupied" << std::endl;
      }
      std::ifstream file (id + ".txt");
    }
    int curr = stoi(data[data.size()-1]);
    // update number of available seats
    data[data.size()-1] = std::to_string(curr - spots);
    std::ofstream eventfile ("arrangement"+ code);
    for(int seat : seats){
      eventfile << seat << std::endl;
    }

  }
  else{
    std::cout << "How many spots :";
    std::cin >> spots;
    int current, nouvo;
    current = stoi(data[data.size()-1]);
    nouvo = current - spots;
    // check if there is sufficient place
    if (nouvo >= 0){
      data[data.size()-1] = std::to_string(nouvo);
    }
    else{
      std::cout << "Only " << current << " seats are available" << std::endl;
    }
  }

  // update files
  std::ofstream fileWrite (filename);
  for(std::string item : data){
    fileWrite << item << std::endl;
  }

  return 0;
}

// menu option 3
void cancel(){
  std::string id, code;
  // fetch id of event to cancel
  std::cout << "Cancel for ID: ";
  std::cin >> id;
  code = id[0];

  if (code == "S"){
    int spots;
    std::string identifier = id+".txt";
    std::ifstream file ("arrangement" + identifier);
    int seats[200];

    std::string spot;
    int i=0;
    while (file >> spot){
      seats[i] = stoi(spot);
      i++;
    }
    std::cout << "Number of spots to cancel :";
    std::cin >> spots;

    std::vector <std::string> choices;
    int choice;
    int j = 0;
    while (j < spots){
      std::cout << "Cancel seat : ";
      std::cin >> choice;
      if (seats[choice-1] == -1){
        seats[choice-1] = choice;
        j++;
      }
      else{
        std::cout << "Seat is already free";
      }
    }
    std::ofstream eventfile ("arrangement"+identifier);
    for(int seat : seats){
      eventfile << seat << std::endl;
    }
    std::ifstream datafile (identifier);
    std::vector <std::string> deuteu;
    std::string row;
    while (datafile >> row){
      std::cout << row << std::endl;
      deuteu.push_back(row);
    }
    deuteu[deuteu.size()-1] = std::to_string(stoi(deuteu[deuteu.size()-1]) - spots);

    std::ofstream deuteufile (identifier);
    for (std::string reuw : deuteu){
      deuteufile << reuw <<std::endl;
    }

  }
  else{
    int num, cncl;
    std::cout << "Cancel how many tickets? :";
    std::cin >> cncl;

    std::string filename;
    filename = id + ".txt";
    std::ifstream eventfile (filename);
    std::vector <std::string> data;

    std::string info;
    while (eventfile >> info){
      std::cout << info << std::endl;
      data.push_back(info);
    }
    num = stoi(data[5]) + cncl;
    data[5] = std::to_string(num);

    std::cout << data[5];

    eventfile.close();

    std::ofstream file (filename);
    for(std::string item : data){
      file << item << std::endl;
    }

  }
  updt();
}
void fetchFiles(){
  std::cout << "Films: " <<std::endl;
  for (Film flm : filmStorage){
    flm.get();
    std::cout << std::endl;
  }
  std::cout << std::endl << std::endl;
  std::cout << "Stand Ups: " << std::endl;
  for (StandUp su : suStorage){
    su.get();
    std::cout << std::endl;
  }
  std::cout << std::endl << std::endl;
  std::cout << "Live Music shows: " << std::endl;
  for (LiveMusic lm : lmStorage){
    lm.get();
    std::cout << std::endl;
  }
}
int menu(){
  updt();
  int choice;
  std::cout << "=========================================" << std::endl;
  std::cout << "1. List all events" << std::endl;
  std::cout << "2. Add booking" << std::endl;
  std::cout << "3. Cancel/refund booking" << std::endl;
  std::cout << "4. Details and availability of an event" << std::endl;
  std::cout << "0. Exit" << std::endl;
  std::cout << "=========================================" << std::endl;
  std::cout << std::endl;

  std::cout << ">>> Choice: ";
  std::cin >> choice;
  std::cout << std::endl;
  return choice;
}



int init(){
  int choice;
  choice = menu();
  if (choice==1){
      list();
  }else if (choice==2){
      addBooking();
  }else if (choice==3){
      cancel();
  }else if (choice==4){
      fetchFiles();
  }else if (choice==0){
      return 0;
  }else{
      std::cout << "Please enter a valid option";
  }
  std::cout << std::endl;

  init();
  return 0;
}


int main(){
  updt();
  init();
}
