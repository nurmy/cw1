#include <iostream>
#include <fstream>
#include <vector>

void fetchFiles(){
  std::ifstream file ("events.txt");
  std::vector <std::string> events;

  std::string event;
  while (file >> event){
    events.push_back(event);
  }
  for(std::string item : events){
    std::string filename = ".txt";
    filename = item + filename;

    std::ifstream eventfile (filename);
    std::vector <std::string> data;
    std::string info;
    while (eventfile >> info){
      data.push_back(info);
    }
    std::string identifier;
    identifier = filename[0];

    std::cout<<"ID: "<<data[0]<<std::endl;
    std::cout<<"Name: "<<data[1]<<std::endl;
    std::cout<<"Start date: "<<data[2]<<std::endl;
    std::cout<<"Number of sessions: "<<data[3]<<std::endl;
    if (identifier == "S"){
      std::cout<<"Comedian: "<<data[4]<<std::endl;
    }
    else if (identifier == "L"){
      std::cout<<"Performer: "<<data[4]<<std::endl;
    }
    else if (identifier == "F"){
      std::cout<<"Director: "<<data[4]<<std::endl;
    }
    int y = stoi(data[5]);

    bool flag = identifier=="L";
    if (flag){
      std::cout<<"Seats remaining: "<<y<<std::endl;
    }
    else{
      std::cout<<"Seats remaining: "<<y<<std::endl;
    }

    std::cout << std::endl;
}
}

void cancel(){
  std::string id, code;
  std::cout<<"Cancel for ID: ";
  std::cin>>id;
  code = id[0];

  if (code=="S"){
    int spots;
    std::string identifier = id+".txt";
    std::ifstream file ("arrangement"+identifier);
    int seats[200];

    std::string spot;
    int i=0;
    while (file >> spot){
      // std::cout << spot<< std::endl;
      seats[i]=stoi(spot);
      i++;
    }
    std::cout<<"Number of spots to cancel :";
    std::cin>>spots;


    std::vector <std::string> choices;
    int choice;
    int j = 0;
    while (j < spots){
      std::cout<<"Cancel seat : ";
      std::cin>>choice;
      if (seats[choice-1]==-1){
          seats[choice-1] = choice;
          j++;
      }
      else{
        std::cout<<"Seat is already free";
      }
    }
    std::ofstream eventfile ("arrangement"+identifier);
    for(int seat : seats){
      eventfile<<seat<<std::endl;
    }

  }
  else{
    int num, cncl;
    std::cout<<"Cancel how many tickets? :";
    std::cin>>cncl;

    std::string filename;
    filename = id + ".txt";
    std::ifstream eventfile (filename);
    std::vector <std::string> data;

    std::string info;
    while (eventfile >> info){
      std::cout << info<< std::endl;
      data.push_back(info);
    }
    num = stoi(data[5]) + cncl;
    data[5] = std::to_string(num);

    std::cout<<data[5];

    eventfile.close();

    std::ofstream file (filename);
    for(std::string item : data){
      file<<item<<std::endl;
    }

  }
}

int menu(){
  int choice;
  std::cout << "1. Add booking"<<std::endl;
  std::cout << "2. Cancel/refund booking"<<std::endl;
  std::cout << "3. List all events"<<std::endl;
  std::cout << "4. Details and availability of an event"<<std::endl;
  std::cout << "0. Exit"<<std::endl;
  std::cout << std::endl;

  std::cout << "Choice: ";
  std::cin >> choice;
  return choice;
}

int addBooking(){
  std::string id;
  int spots;
  std::cout<<"ID to book :";
  std::cin>>id;
  std::string filename = id + ".txt";
  std::ifstream eventfile (filename);
  std::vector <std::string> data;
  std::string info;
  while (eventfile >> info){
    data.push_back(info);
  }
  std::cout<<data[data.size()-1]<<" spots available"<<std::endl;
  std::cout<<"Continue? ";
  std::string response;
  std::cin >> response;
  if (response=="no"){
    return 0;
  }
  if (id[0]=='S'){
    std::string code = id+".txt";
    std::ifstream file ("arrangement"+code);
    int seats[200];

    std::string spot;
    int i=0;
    while (file >> spot){
      // std::cout << spot<< std::endl;
      seats[i]=stoi(spot);
      i++;
    }
    std::cout<<"How many spots :";
    std::cin>>spots;


    std::vector <std::string> choices;
    int choice;
    int j = 0;
    while (j < spots){
      std::cin>>choice;
      if (seats[choice-1]==choice){
          seats[choice-1] = -1;
          j++;
      }
      else{
        std::cout<<"Seat is already occupied";
      }
    }
    std::ofstream eventfile ("arrangement"+code);
    for(int seat : seats){
      eventfile<<seat<<std::endl;
    }

  }
  else{
    std::cout<<"How many spots :";
    std::cin>>spots;
    int current, nouvo;
    current = stoi(data[data.size()-1]);
    nouvo = current - spots;
    if (nouvo>=0){
      data[data.size()-1] = std::to_string(nouvo);
      std::ofstream fileWrite (filename);
      for(std::string item : data){
        fileWrite<<item<<std::endl;
      }
    }
    else{
      std::cout<<"Only "<<current<<" seats are available"<<std::endl;
    }
  }

  return 0;
}
void list(){
  std::ifstream file ("events.txt");
  std::vector <std::string> events;

  std::string event;
  while (file >> event){
    events.push_back(event);
  }
  for(std::string item : events){
    std::string filename = ".txt";
    filename = item + filename;

    std::ifstream eventfile (filename);
    std::vector <std::string> data;
    std::string info;
    while (eventfile >> info){
      data.push_back(info);
    }

    std::cout<<"Name: "<<data[1]<<std::endl;
    if (filename[0]=='S'){
      std::cout<<"Comedian: "<<data[4]<<std::endl;
    }
    else if(filename[0]=='L'){
      std::cout<<"Artists: "<<data[4]<<std::endl;
    }
    else if (filename[0]=='F'){std::cout<<"Director: "<<data[4]<<std::endl;}
    int y = stoi(data[5]);
    bool flag = filename[0]=='L';

    if (filename[0]=='L'){
      std::cout<<"Seats remaining: "<<y<<std::endl;
    }
    else{
      std::cout<<"Seats remaining: "<<y<<std::endl;
    }

    std::cout << std::endl;
}
}

int main()
{
  int choice;
  choice = menu();
  switch (choice) {
    case 1 :
      addBooking();
      break;
    case 2 :
      cancel();
      break;
    case 3 :
    list();
      break;
    case 4 :
      fetchFiles();
      break;
    case 0 :
      return 0;
    default:
      std::cout<<"Please enter a valid option";
  }
  std::cout<<std::endl;
  main();
}
